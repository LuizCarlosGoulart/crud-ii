O segundo CRUD foi planejado para uma empresa que teria uma necessidade prática para um banco de dados, e 
que vai usufruir de criações de tabelas e arquivos em json, sendo possível utilizar outros softwares externos
como insomnia ou postman para manusear o banco de dados

A segunda solução foi criar um CRUD que vai gerar dados mais brutos, como formulário em JSON, mas que ainda tenha
o mesmo funcionamento de qualquer outro banco de dados, você pode gerenciar este banco utilizando softwares
 externos como postman ou insomnia para ser mais intuitivo, mas ainda assim é possível alterá-lo na página web
