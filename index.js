// config inicial

const mongoose = require('mongoose')
const express = require('express')
const app = express()
const { response } = require('express')

// leitura do JSON
app.use(
    express.urlencoded({
        extended: true
    })
)

app.use(express.json())

// Rotas
const personRoutes = require('./routes/personRoutes')
app.use('/person', personRoutes)

// endpoint
app.get('/', (req, res) => {
    res.json({message: 'Teste'})
})

const DB_USER = 'user'
const DB_PASSWORD = encodeURIComponent('123')

// entregar uma porta
mongoose.connect(
    `mongodb+srv://${DB_USER}:${DB_PASSWORD}@apicluster.9t2bt.mongodb.net/apirest?retryWrites=true&w=majority`
    )
.then(() => {
    console.log('Conectado ao Servidor')
    app.listen(3000)
})
.catch((err) => console.log(err))